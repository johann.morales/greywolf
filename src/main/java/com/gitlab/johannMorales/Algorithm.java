package com.gitlab.johannMorales;

import com.gitlab.johannMorales.exception.InvalidParameterException;
import com.gitlab.johannMorales.model.Pack;
import com.gitlab.johannMorales.model.Wolf;
import org.la4j.Vector;

import java.util.Random;

public class Algorithm<T extends Wolf> {

    private Integer iterations;
    private Integer size;
    private Vector a;
    private Vector A;
    private Vector C;
    private Pack<T> pack;

    public void run() {
        for (int iteration = 0; iteration < iterations; iteration++) {
            Vector r1 = Vector.random(T.dimensions, new Random());
            Vector r2 = Vector.random(T.dimensions, new Random());

            this.a = Vector.constant(T.dimensions, 2 - iteration * (2 / iterations));
            this.A = a.multiply(2.0).hadamardProduct(r1).subtract(a);
            this.C = r2.multiply(2.0);

            pack.getWolfs().forEach(wolf -> {
                Vector oldPosition = wolf.getPosition();

                T alpha = pack.getAlpha();
                T beta = pack.getBeta();
                T delta = pack.getDelta();

                Vector alphaDistance = calculateDistanceToWolf(alpha, oldPosition);
                Vector betaDistance = calculateDistanceToWolf(beta, oldPosition);
                Vector deltaDistance = calculateDistanceToWolf(delta, oldPosition);

                Vector alphaInfluence = calculateInfluence(alpha, alphaDistance);
                Vector betaInfluence = calculateInfluence(beta, betaDistance);
                Vector deltaInfluence = calculateInfluence(delta, deltaDistance);

                Vector newPosition = (alphaInfluence.add(betaInfluence).add(deltaInfluence)).divide(3.0);

                wolf.setPosition(newPosition);
            });

            pack.reorderHierarchy();
        }
    }

    private Vector calculateDistanceToWolf(T wolf, Vector from) {
        return C.hadamardProduct(wolf.getPosition()).subtract(from);
    }

    private Vector calculateInfluence(T wolf, Vector distance) {
        return wolf.getPosition().subtract(A.hadamardProduct(distance));
    }

    public Algorithm(Integer iterations, Integer size) throws InvalidParameterException {

        if (iterations <= 0) {
            throw new InvalidParameterException();
        }

        if (size < 4) {
            throw new InvalidParameterException();
        }

        this.iterations = iterations;
        this.size = size;
        this.pack = new Pack<T>();
    }

}
