package com.gitlab.johannMorales.model;

import com.gitlab.johannMorales.exception.FullPackException;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Getter
public class Pack<T extends Wolf> {

    private T alpha;
    private T beta;
    private T delta;
    private List<T> omega;
    private List<T> wolfs;
    private Integer size;

    public static Pack create(Integer size) {
        Pack pack = new Pack<Wolf>();
        pack.size = size;
        pack.wolfs = new ArrayList<>(size);
        pack.omega = new ArrayList<>(size - 2);
        pack.beta = null;
        pack.alpha = null;
        pack.delta = null;
        return pack;
    }

    public void addWolf(T wolf) {
        if(wolfs.size() == size) {
            throw new FullPackException();
        } else {
            this.wolfs.add(wolf);
        }
    }

    /**
     * Calculates new fitness for all wolfs in pack and sets new alpha, beta and delta according to these new values
     */
    public void reorderHierarchy() {
        for (T wolf : this.wolfs) {
            wolf.calculateFitness();
        }


        T possibleAlpha = null;
        Double possibleAlphaFitness = Double.MIN_VALUE;

        T possibleBeta = null;
        Double possibleBetaFitness = Double.MIN_VALUE;

        for (T wolf : this.wolfs) {
            if(wolf.getFitness() > possibleAlphaFitness) {
                possibleAlpha = wolf;
            } else if ( wolf.getFitness() > possibleBetaFitness) {
                possibleBeta = wolf;
            }
        }

        this.alpha = possibleAlpha;
        this.beta = possibleBeta;
    }

}
