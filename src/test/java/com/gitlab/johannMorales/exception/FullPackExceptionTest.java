package com.gitlab.johannMorales.exception;

import static org.junit.jupiter.api.Assertions.assertSame;

public class FullPackExceptionTest {

    public void testCreation() {
        Exception exception = new FullPackException();
        assertSame(exception.getMessage(), FullPackException.ERROR_MSG);
    }

}