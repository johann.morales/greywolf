package com.gitlab.johannMorales.model;

import com.gitlab.johannMorales.exception.FullPackException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PackTest {

    public static Integer SIZE = 20;
    private Pack pack;

    @BeforeEach
    void setUp() {
        pack = Pack.create(SIZE);
    }

    @Test
    public void testCreateNull() {
        assertNull(pack.getAlpha());
        assertNull(pack.getBeta());
        assertNotNull(pack.getOmega());
        assertNotNull(pack.getWolfs());
        assertNotNull(pack.getSize());
    }

    @Test
    public void testCreateValues() {
        assertSame(pack.getSize(), SIZE);
        assertSame(pack.getWolfs().size(), 0);
        assertSame(pack.getOmega().size(), 0);
    }

    @Test
    public void testAddWolf() {
        for (int i = 0; i < SIZE; i++) {
            //pack.addWolf(new Wolf());
        }

        Boolean throwsExceptionIfFull = false;

        try {
            //pack.addWolf(new Wolf());
        }catch (FullPackException ex) {
            throwsExceptionIfFull = true;
        }

        assertTrue(throwsExceptionIfFull);
    }
}