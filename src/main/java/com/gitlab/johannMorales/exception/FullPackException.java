package com.gitlab.johannMorales.exception;

public class FullPackException extends RuntimeException {

    public static String ERROR_MSG = "Pack is full!";

    public FullPackException() {
        super(ERROR_MSG);
    }
}
