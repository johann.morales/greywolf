package com.gitlab.johannMorales.model;

import lombok.Getter;
import lombok.Setter;
import org.la4j.Vector;

@Getter
public class Wolf {

    public static Integer dimensions = 1;
    private Double fitness;

    @Setter
    private Vector position;


    private Wolf() {
    }

    public static Wolf generate() {
        Wolf wolf = new Wolf();
        return wolf;
    }

    public void calculateFitness() {
    }
}
